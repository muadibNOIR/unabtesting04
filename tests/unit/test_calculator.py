import unittest
import math
from app.calculator import Calculator


class TestCalculatorInput(unittest.TestCase):
    def test_input_string(self):
        result = Calculator(1, "a")
        self.assertRaises(TypeError, result)


class TestCalculatorAdd(unittest.TestCase):
    def test_add_positive(self):
        result = Calculator(100, 1).suma()
        self.assertEqual(result, 101)

    def test_add_negative(self):
        result = Calculator(-1, -100).suma()
        self.assertEqual(result, -101)


class TestCalculatorDivide(unittest.TestCase):
    def test_divide_positive(self):
        result = Calculator(4, 2).divide()
        self.assertEqual(result, 2)

    def test_divide_byzero(self):
        result = Calculator(4, 0).divide()
        self.assertRaisesRegex(ZeroDivisionError, result)


class TestCalculatorResta(unittest.TestCase):
    def test_resta_positivos(self):
        result = Calculator(100, 1).resta()
        self.assertEqual(result, 99)

    def test_resta_negativos(self):
        result = Calculator(-100, -1).resta()
        self.assertEqual(result, -99)


class TestCalculatorElevado(unittest.TestCase):
    def test_elevado_cero(self):
        result = Calculator(6, 0).elevado()
        self.assertEqual(result, 1)

class TestCalculatorRaiz(unittest.TestCase):
    def test_raiz_indiceCero(self):
        result = Calculator(8,0).raiz()
        self.assertRaisesRegex(ZeroDivisionError, result)

class TestCalculatorLog(unittest.TestCase):
    def test_logaritmo_baseCero(self):
        result = Calculator(100,0).logaritmo()
        self.assertRaisesRegex(ValueError, result)

    def test_logaritmo_baseUno(self):
        result = Calculator(100,1).logaritmo()
        self.assertRaisesRegex(ZeroDivisionError, result)

