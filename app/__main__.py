from calculator import Calculator

while True:
    print("#### CALCULATOR ####")
    try:
        num1 = int(input("Enter first number: "))
        num2 = int(input("Enter second number: "))

        numeros = Calculator(num1, num2)

        print("Select operation.")
        print("1. Add")
        print("2. Subtract")
        print("3. Multiply")
        print("4. Divide")
        print("5. num1^num2")
        print("6. Raiz de indice num2 de num1")
        print("7. Logaritmo num1 en base num2")

        choice = input("Enter choice(1/2/3/4/5/6/7):\n")

        result = 0

        if choice == '1':
            result = numeros.suma()
        elif choice == '2':
            result = numeros.resta()
        elif choice == '3':
            result = numeros.multi()
        elif choice == '4':
            result = numeros.divide()
        elif choice == '5':
            result = numeros.elevado()
        elif choice == '6':
            result = numeros.raiz()
        elif choice == '7':
            result = numeros.logaritmo()
        else:
            print("Invalid Input")

        print("Result = ", result)

    except ValueError:
        print("Por favor ingrese un caracter válido\n")

    quit = input("Do you want to continue (y/n) ?")
    if quit == 'n':
        break
