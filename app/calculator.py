import math


class Calculator:

    def __init__(self, x, y):
        try:
            self.x = x
            self.y = y
        except TypeError:
            print("Por favor ingrese un entero")

    def suma(self):
        return self.x + self.y

    def resta(self):
        return self.x - self.y

    def multi(self):
        return self.x * self.y

    def divide(self):
        try:
            return self.x / self.y
        except ZeroDivisionError:
            print("El divisor (num2) no puede ser 0\n")

    def elevado(self):
        return self.x ** self.y

    def raiz(self):
        try:
            return (self.x ** (1/self.y))
        except ZeroDivisionError:
            print("el indice de la raiz (num2) no puede ser 0\n")

    def logaritmo(self):
        try:
            return (math.log(self.x, self.y))
        except ZeroDivisionError:
            print("la base (num2) no puede ser 1\n")
        except ValueError:
            print("la base (num2) no puede ser 0\n")
